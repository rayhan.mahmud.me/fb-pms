<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/webhook', 'BackEndController@verify_token');
Route::post('/webhook', 'BackEndController@verify_token');

Route::group(['prefix' => 'admin','middleware'=>['auth','admin']], function () {
    Route::get('dashboard', 'BackEndController@Dashboard')->name('admin.dashboard');

    Route::get('sms-setting', 'SmsSettingController@smsSetting')->name('smsSetting');
    Route::post('update-sms-setting', 'SmsSettingController@updateSmsSetting')->name('updateSmsSetting');



    Route::get('general-setting', 'GeneralSettingController@index')->name('general_setting');
    Route::POST('update-general-setting', 'GeneralSettingController@update')->name('update-general-setting');
    Route::POST('update-logo', 'GeneralSettingController@updateLogo')->name('update-logo');
    Route::POST('update-fav', 'GeneralSettingController@updateFav')->name('update-fav');

    // pms start

    Route::get('app-setting', 'GeneralSettingController@appSetting')->name('appSetting');
    Route::post('update-app-setting', 'GeneralSettingController@updateAppSetting')->name('updateAppSetting');

    // Route::resource('post', 'PostController');
    Route::get('post', 'PostController@index')->name('post.index');
    Route::get('post/create', 'PostController@create')->name('post.create');
    Route::post('post/store', 'PostController@store')->name('post.store');
    Route::get('post/{id}/edit', 'PostController@edit')->name('post.edit');
    Route::post('post/update/{id}', 'PostController@update')->name('post.update');
    Route::get('post/destroy/{id}', 'PostController@destroy')->name('post.destroy');

    // Route::get('already-posted', 'PostController@alreadyPosted')->name('alreadyPosted');
    Route::get('already-posted', 'GraphController@getAllPost')->name('alreadyPosted');
    Route::get('delete-already-posted/{id}', 'GraphController@deleteAlreadyPosted')->name('deleteAlreadyPosted');
    Route::get('fb-post-details/{post_id}', 'GraphController@postDetails')->name('fbPostDetails');
    Route::get('individual-photo-details/{id}', 'GraphController@individualPhotoDetails')->name('individualPhotoDetails');

    Route::post('post-comment', 'GraphController@postCommentStore')->name('postCommentStore');
    Route::post('comment-reply', 'GraphController@commentReplyStore')->name('commentReplyStore');
    Route::get('delete-comment/{id}', 'GraphController@deleteComment')->name('deleteComment');


    Route::get('user-info-get/{user_id}', 'GraphController@userInfoGet')->name('userInfoGet');
    Route::get('page-info-get', 'GraphController@pageInfoGet')->name('pageInfoGet');

    Route::get('post-details/{post_id}', 'PostController@postDetails')->name('postDetails');



    Route::post('/getall', 'PostController@getall')->name('getall');
    Route::post('/getmodal', 'PostController@getmodal')->name('getmodal');

    Route::resource('profile', ProfileController::class);
    // Route::get('profile', 'ProfileController@index')->name('profile');
    Route::get('/facebook', 'ProfileController@redirectToFacebookProvider')->name('facebook');
    Route::get('facebook/callback', 'ProfileController@handleProviderFacebookCallback');
    Route::post('facebook_page_id', 'ProfileController@facebook_page_id')->name('facebook_page_id');


    Route::post('publish-to-page', 'GraphController@publishToPage')->name('publishToPage');

    Route::prefix('apps')->group(function() {
        Route::get('/', 'FacebookAppController@index')->name('apps');
        Route::post('/store','FacebookAppController@store')->name('apps.store');
        Route::post('/update','FacebookAppController@update')->name('apps.update');
        Route::get('/edit/{id}','FacebookAppController@edit')->name('apps.edit');
        Route::get('/delete/{id}','FacebookAppController@destroy')->name('apps.delete');
        Route::post('/update-active-status','FacebookAppController@update_active_status')->name('apps.update_active_status');
    });
    Route::prefix('fb-page')->group(function() {
        Route::get('/', 'FacebookPageController@index')->name('fbPage');
        Route::post('/store','FacebookPageController@store')->name('fbPage.store');
        Route::post('/update','FacebookPageController@update')->name('fbPage.update');
        Route::get('/edit/{id}','FacebookPageController@edit')->name('fbPage.edit');
        Route::get('/delete/{id}','FacebookPageController@destroy')->name('fbPage.delete');
        Route::post('/update-active-status','FacebookPageController@update_active_status')->name('fbPage.update_active_status');
    });

});



Route::group(['middleware'=>['auth','userVerified']], function () {
    // Profile
    Route::get('profile', 'GeneralSettingController@Profile');
    Route::POST('update-profile-image', 'GeneralSettingController@updateprofileImage')->name('update-profile-image');
    Route::POST('update-profile-info', 'GeneralSettingController@updateprofileInfo')->name('update-profile-info');
    Route::post('change-pass', 'GeneralSettingController@passwordUpdate');

});
    Route::get('/verification', 'OTPVerificationController@verification')->name('verification');
    Route::post('/verification', 'OTPVerificationController@verify_phone')->name('verification.submit');
    Route::get('/verification/phone/code/resend', 'OTPVerificationController@resend_verificcation_code')->name('verification.phone.resend');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::any('password/reset/phone', 'OTPVerificationController@sendResetCode')->name('password.phone');
    Route::post('/password/reset/phone/submit', 'OTPVerificationController@reset_password_with_code')->name('password.update');
