<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('full_picture')->nullable();
            $table->string('post_type')->nullable()->default('text');
            $table->string('video')->nullable();
            $table->string('file_type')->nullable();
            $table->text('message')->nullable();
            $table->tinyInteger('status')->nullable()->default(0)->comments('1. active, 2. inactive');
            $table->string('fb_id')->nullable();
            $table->string('fb_post_id')->nullable();
            $table->string('fb_page_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
