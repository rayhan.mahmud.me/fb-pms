<?php

use App\FacebookPage;
use Illuminate\Database\Seeder;

class FacebookPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $app =new FacebookPage();
        $app->name = 'E-Shop';
        $app->page_id = '111861468008931';
        $app->save();
    }
}
