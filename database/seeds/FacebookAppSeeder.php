<?php

use App\FacebookApp;
use Illuminate\Database\Seeder;

class FacebookAppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $app =new FacebookApp();
        $app->name = 'E-Shop';
        $app->app_id = '447114413657270';
        $app->save();

    }
}
