<?php

use FacebookAppSeeder;
use FacebookPageSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FacebookAppSeeder::class);
        $this->call(FacebookPageSeeder::class);
    }
}
