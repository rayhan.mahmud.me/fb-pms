<?php

namespace App\Http\Controllers;

use App\FacebookPage;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Validator;

class FacebookPageController extends Controller
{
    public function index()
    {
        $pages = FacebookPage::all();
        return view('admin.facebook_pages.index',compact('pages'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'page_id' => 'required|unique:facebook_pages,name',
        ]);

        $messages = $validator->messages();
        foreach ($messages->all() as $message)
        {
            Toastr::error($message, 'Failed', ['timeOut' => 2000]);
        }

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $page = new FacebookPage();
        $page->name = $request->name;
        $page->page_id = $request->page_id;
        $page->save();

        Toastr::success(__('Page Added Successfully'), 'Success', ['timeOut' => 2000]);
        return redirect()->route('fbPage');
    }

    // public function edit($id)
    // {
    //     $skill = FacebookApp::find($id);
    //     return response()->json($skill);
    // }
    // public function update(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'name' => 'required|unique:skills,name,'. $request->id,
    //         'percentage' => 'required',
    //     ]);

    //     $messages = $validator->messages();
    //     foreach ($messages->all() as $message)
    //     {
    //         Toastr::error($message, 'Failed', ['timeOut' => 2000]);
    //     }

    //     if ($validator->fails()) {
    //         return redirect()->back()
    //             ->withErrors($validator)
    //             ->withInput();
    //     }

    //     $skill = FacebookApp::findOrFail($request->id);
    //     $skill->name = $request->name;
    //     $skill->percentage = $request->percentage;
    //     $skill->save();

    //     Toastr::success(__('Skill Update Successfully'), 'Success', ['timeOut' => 2000]);
    //     return redirect()->back();
    // }

    // public function update_active_status(Request $request)
    // {
    //     $skill = FacebookApp::findOrFail($request->id);
    //     $skill->active_status = $request->status;
    //     if($skill->save()){
    //         return 1;
    //     }
    //     return 0;
    // }

    // public function destroy($id)
    // {
    //     $skill = FacebookApp::where('id', $id)->first();
    //     $skill->delete();
    //     Toastr::success(__('Skill Delete Successfully'), 'Success', ['timeOut' => 2000]);
    //     return redirect()->back();
    // }
}
