<?php

namespace App\Http\Controllers;

use App\User;
use App\Event;
use pimax\FbBotApp;
use pimax\Messages\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BackEndController extends Controller
{
    public function Dashboard(Request $request){
        // dd($request->all());
        $users = User::where('role_id',2)->latest()->take(5)->get();

        return view('admin.dashboard',compact('users'));
    }
    public function webhook(){
        $local_verify_token = env('WEBHOOK_VERIFY_TOKEN');
        $hub_verify_token = \Input::get('hub_verify_token');
        if ($hub_verify_token == $local_verify_token) {
            return \Input::get('hub_challenge');
        }else {
            return 'Invalid verify token';
        }
    }
    public function verify_token(Request $request)
    {
        try {
            $mode  = $request->get('hub_mode');
            $token = $request->get('hub_verify_token');
            Log::info('mode: '.$mode);
            Log::info('token: '.$token);

            if ($mode && $token === env('VERIFY_TOKEN') && $request->get('hub_challenge')) {
                Log::info('challange: '.$request->get('hub_challenge'));
                return response($request->get('hub_challenge'))->header('Content-Type', 'text/plain');

            }

            $data = $request->all();
            $id = $data['entry'][0]['messaging'][0]['sender']['id'];
            $message = $data['entry'][0]['messaging'][0]['message']['text'];
            Log::info("message:". $message);
            if(!empty($message)){
                $this->send_message($id, 'Hello Rayhan');
            }

            // return response("Invalid token!", 400);
        } catch (\Throwable $th) {
            Log::info($th->getMessage());
        }

    }

    public function send_message($id, $message)
        {
            Log::info("token: " . env('PAGE_ACCESS_TOKEN'));
            $url = 'https://graph.facebook.com/v2.6/me/messages?access_token='.env('PAGE_ACCESS_TOKEN');
            $data = [
                'recipient' => [
                    'id' => $id
                ],
                'message' => [
                    'text' => $message
                ]
            ];
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_exec($ch);
            curl_close($ch);
        }
}
