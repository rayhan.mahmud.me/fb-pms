<?php

namespace App\Http\Controllers;

use App\Post;
use Exception;

use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\GraphController;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::orderby('id', 'desc')->where('fb_page_id',Auth::user()->facebook_page_id)->paginate(10);
        return view('admin.post.index',compact('posts'));
    }

    public function create()
    {
        return view('admin.post.post_create');
    }
    public function getmodal(Request $request)
    {
        $data = array();
        if (isset($request->id) && $request->id != '') {
            $id = decrypt($request->id);
            $data = Post::where('id',$id)->first();
        }

        return view('post.getmodal', compact('data'));
    }

    // public function store(Request $request)
    // {
    //     // dd($request->all());

    //     $request->validate([
    //         'name' => 'required',
    //         'message' => 'required',
    //         'image' => 'required',
    //     ]);
    //     $post = new Post();
    //     $post->name = $request->name;
    //     $post->message = $request->message;
    //     $image = "";
    //     if ($request->file('image') != "") {
    //         $file = $request->file('image');
    //         $image = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
    //         $file->move('public/backend/uploads/post/', $image);
    //         $image = 'public/backend/uploads/post/' . $image;
    //         $post->image = $image;
    //     }
    //     $post->save();

    //     Toastr::success('Operation successful', 'Success');
    //     return redirect()->route('post.index');

    // }
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required',
        ]);

        $post = new Post();
        $post->name = $request->name;
        $post->message = $request->message;
        $post->post_type = $request->post_type;

        if ($request->post_type == 'photo') {
            if($request->hasfile('filenames'))
            {
                foreach($request->file('filenames') as $file)
                {
                    $name = md5($file->getClientOriginalName() . time()) .'.'.$file->extension();
                    $file->move(public_path().'/backend/uploads/post/images/', $name);
                    $data[] = 'backend/uploads/post/images/' . $name;

                    // $name = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                    // $file->move('public/backend/uploads/post/images/', $name);
                    // $image = 'backend/uploads/post/images/' . $name;
                    // $data[] = $name;

                }
                $post->image=json_encode($data);
            }
        }



        // $image = "";
        // if ($request->file('image') != "") {
        //     $file = $request->file('image');
        //     $image = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        //     $file->move('public/backend/uploads/post/images/', $image);
        //     $image = 'backend/uploads/post/images/' . $image;
        //     $post->image = $image;

        //     $extension =$file->getClientOriginalExtension();
        //     $post->file_type = $extension;
        // }
        if ($request->post_type == 'video') {
            $video = "";
            if ($request->file('video') != "") {
                $file = $request->file('video');
                $video = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move(public_path().'/backend/uploads/post/videos/', $video);
                $video = 'backend/uploads/post/videos/' . $video;
                $post->video = $video;

                $extension =$file->getClientOriginalExtension();
                $post->file_type = $extension;
            }
        }
        $post->fb_page_id = Auth::user()->facebook_page_id;
        $post->save();

        Toastr::success('Operation successful', 'Success');
        return redirect()->route('post.index');

    }
    // public function store(Request $request)
    // {
    //     $input = $request->all();
    //     if ($request->hasFile('image') || !empty($input['name']) || !empty($input['message']) ) {
    //         try {
    //             if (isset($request->id)) {
    //                 $id = decrypt($request->id);
    //                 $msg =  'updated successfully';
    //                 $data = Post::find($id);
    //                 if ($request->hasFile('image')) {
    //                     if(file_exists(public_path('images/'.$data->image)) && $data->image!='') {
    //                         unlink(public_path('images/'.$data->image));
    //                     }
    //                 }
    //             }else{
    //                 $msg =  'Added successfully';
    //                 $data = new Post;
    //             }
    //             if ($request->hasFile('image')) {

    //                 $destinationPath = public_path().'/images/';
    //                 $file = $input['image'];
    //                 $fileName = rand(11111, 99999) . '_'.$file->getClientOriginalName();
    //                 $extension =$file->getClientOriginalExtension();

    //                 $file->move($destinationPath, $fileName);
    //                 //dd($fileName);
    //                 $data->image = $fileName;
    //                 $data->file_type = $extension;

    //             }
    //             $data->name = $input['name'];
    //             $data->message = $input['message'];
    //             $data->save();
    //             $arr = array("status" => 200, "msg" => $msg);

    //         } catch (\Illuminate\Database\QueryException $ex) {
    //             $msg = $ex->getMessage();
    //             if (isset($ex->errorInfo[2])) :
    //                 $msg = $ex->errorInfo[2];
    //             endif;
    //             $arr = array("status" => 400, "msg" => $msg, 'line'=> $ex->getLine(), "result" => array());
    //         }catch (Exception $ex) {
    //             $msg = $ex->getMessage();
    //             if (isset($ex->errorInfo[2])) :
    //                 $msg = $ex->errorInfo[2];
    //             endif;
    //             $arr = array("status" => 400, "msg" => $msg, 'line'=> $ex->getLine(), "result" => array());
    //         }
    //     }
    //     return \Response::json($arr);
    // }

    public function edit($id)
    {
        $editData = Post::where('id',$id)->first();
        return view('admin.post.post_create',compact('editData'));
    }

    public function update(Request $request,$id)
    {
        // dd($request->all());

        $request->validate([
            'name' => 'required',
        ]);
        $post = Post::where('id',$id)->first();
        $post->name = $request->name;
        $post->message = $request->message;

        // $image = "";
        // if ($request->file('image') != "") {
        //     $file = $request->file('image');
        //     $image = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        //     $file->move('public/backend/uploads/post/images/', $image);
        //     $image = 'backend/uploads/post/images/' . $image;
        //     $post->image = $image;

        //     $extension =$file->getClientOriginalExtension();
        //     $post->file_type = $extension;
        // }
        // $video = "";
        // if ($request->file('video') != "") {
        //     $file = $request->file('video');
        //     $video = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        //     $file->move('public/backend/uploads/post/videos/', $video);
        //     $video = 'backend/uploads/post/videos/' . $video;
        //     $post->video = $video;

        //     $extension =$file->getClientOriginalExtension();
        //     $post->file_type = $extension;
        // }
        $post->save();

        Toastr::success('Operation successful', 'Success');
        return redirect()->route('post.index');

    }
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();

        Toastr::success('Operation successful', 'Success');
        return redirect()->back();
    }

    public function alreadyPosted()
    {
        try {
            $grap = new GraphController;
            $params = "posts,message";
            $page_id = 111861468008931;
            $response = $this->api->get('/'.$page_id,'?fields='.$params)->getGraphUser();
            dd($response);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/v12.0/'.$page_id.'?fields=posts%7Bcomments%7Bcreated_time%2Cfrom%2Cmessage%7D%2Cfull_picture%2Cmessage%2Ccreated_time%2Cfrom%2Creactions%7D&access_token='.Auth::user()->token);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $result = curl_exec($ch);
            $data = json_decode($result);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            $posts = $data->posts->data;
            return view('admin.post.index',compact('posts'));
        } catch (\Throwable $th) {
            dd($th);
        }

    }
    public function postDetails($post_id)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/v12.0/'.$post_id.'?fields=full_picture%2Ccreated_time%2Cmessage%2Ccomments%7Bcreated_time%2Cfrom%2Cmessage%7D&access_token='.Auth::user()->token);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $postDetail = json_decode($result);
        // dd($postDetail);

        return view('admin.post.post_details',compact('postDetail'));
    }

    // public function postCommentStore(Request $request)
    // {
    //     // dd( $request->all());
    //     try {
    //         $ch = curl_init();

    //         curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/v12.0/'.$request->post_id.'/comments?message='.$request->comment.'&access_token='.Auth::user()->token);
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //         curl_setopt($ch, CURLOPT_POST, 1);

    //         $result = curl_exec($ch);
    //         if (curl_errno($ch)) {
    //             echo 'Error:' . curl_error($ch);
    //         }
    //         curl_close ($ch);


    //         Toastr::success('Operation successful', 'Success');
    //         return redirect()->back();
    //     } catch (\Throwable $th) {
    //         dd($th);
    //     }
    // }
    // public function commentReplyStore(Request $request)
    // {
    //     // dd( $request->all());
    //     try {

    //             $ch = curl_init();

    //             curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/v12.0/'.$request->comment_id.'/comments?message='.$request->reply.'&access_token='.Auth::user()->token);
    //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //             curl_setopt($ch, CURLOPT_POST, 1);

    //             $result = curl_exec($ch);
    //             if (curl_errno($ch)) {
    //                 echo 'Error:' . curl_error($ch);
    //             }
    //             // dd($ch);
    //             curl_close ($ch);


    //         Toastr::success('Operation successful', 'Success');
    //         return redirect()->back();
    //     } catch (\Throwable $th) {
    //         dd($th);
    //     }
    // }
    // public function deleteComment($id){
    //         $ch = curl_init();

    //         curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/v12.0/'.$id.'?access_token='.Auth::user()->token);
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

    //         $result = curl_exec($ch);
    //         if (curl_errno($ch)) {
    //             echo 'Error:' . curl_error($ch);
    //         }
    //         curl_close ($ch);
    //         Toastr::success('Operation successful', 'Success');
    //         return redirect()->back();

    // }

}
