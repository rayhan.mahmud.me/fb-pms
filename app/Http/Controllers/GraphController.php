<?php

namespace App\Http\Controllers;

use App\Post;
use Exception;
use Facebook\Facebook;
use Goutte\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException;

class GraphController extends Controller
{
    private $api;
    public function __construct(Facebook $fb)
    {
        $this->middleware(function ($request, $next) use ($fb) {
            $fb->setDefaultAccessToken(Auth::user()->token);
            $this->api = $fb;
            return $next($request);
        });
    }

    public function retrieveUserProfile(){
        try {

            $params = "first_name,last_name,age_range,gender";

            $user = $this->api->get('/me?fields='.$params)->getGraphUser();

        } catch (FacebookSDKException $e) {

        }

    }

    public function publishToProfile(Request $request){
        try {
            $id = decrypt($request->id);
            $getdata = Post::find($id);

            if (!empty($getdata)) {
                $response = $this->api->post('/me/feed', [
                    'message' => $getdata->message
                ])->getGraphNode()->asArray();
                if($response['id']){
                    dump($response['id']);
                    // dd($response);
                    // post created
                }
            }else {

                dd('record not found');
            }
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }

    public function getPageAccessToken($page_id){
        try {
             // Get the \Facebook\GraphNodes\GraphUser object for the current user.
             // If you provided a 'default_access_token', the '{access-token}' is optional.
             $response = $this->api->get('/me/accounts', Auth::user()->token);
        } catch(FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        try {
            $pages = $response->getGraphEdge()->asArray();
            foreach ($pages as $key) {
                if ($key['id'] == $page_id) {
                    return $key['access_token'];
                }
            }
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }

    function uploadPhoto($getdata)
    {
        try {
            // dd($getdata);
            $photoIdArray = array();
            $page_id = Auth::user()->facebook_page_id??'';
            foreach(json_decode($getdata->image) as $key =>  $img) {
                //check file exist
                $file_path='public/'.$img;
                if (file_exists($file_path)) {
                    $mime = mime_content_type($file_path);
                    $file_type=explode('/',$mime);
                    $file_type=$file_type[0];
                    if ($file_type == 'image') {
                        $photoId = $this->api->post('/'.$page_id.'/photos', [
                            'url' => asset('public/'.$img),
                            'published' => false
                        ],$this->getPageAccessToken($page_id))->getGraphNode()->asArray();
                    } else {
                        // dd(asset('public/'.$img));
                        $photoId = $this->api->post('/'.$page_id.'/videos', [
                            'file_url' => asset('public/'.$img),
                            'published' => false
                        ],$this->getPageAccessToken($page_id))->getGraphNode()->asArray();

                    }
                }


                $photoIdArray[] = $photoId["id"];
                // dd($photoIdArray);
            }
            return $photoIdArray;
        } catch (\Throwable $th) {
            dd($th);

        }

    }

    function publishMultiPhotoStory($caption, $photoIdArray)
    {
        $page_id = Auth::user()->facebook_page_id??'';
            $params = array();
            $params = array( "message" => $caption );
            if ($photoIdArray != null) {
                foreach($photoIdArray as $k => $photoId) {
                    $params["attached_media"][$k] = '{"media_fbid":"' . $photoId . '"}';
                }
            }

            try {
                // dd($params);
                $postResponse = $this->api->post("/".$page_id."/feed", $params,$this->getPageAccessToken($page_id))->getGraphNode()->asArray();
                return $postResponse;
            } catch (FacebookResponseException $e) {
                dd($e); // When Graph returns an error
                // display error message
                print $e->getMessage();
                exit();
            } catch (FacebookSDKException $e) {
                print $e->getMessage();
                exit();
            }

    }

    function publishVideoStory($getdata)
    {
        $page_id = Auth::user()->facebook_page_id??'';
        try {
            $post = $this->api->post('/' . $page_id . '/videos' , array('description' => $getdata->message, 'source' => $this->api->fileToUpload(public_path($getdata->video))), $this->getPageAccessToken($page_id));
            return $post->getGraphNode()->asArray();
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }
    // function publishVideoStory($getdata)
    // {
    //     $page_id = Auth::user()->facebook_page_id??'';
    //     try {
    //         $videoId = $this->api->post('/'.$page_id.'/videos', [
    //             'description' => $getdata->message,
    //             'source' => asset('public/'.$getdata->video),
    //             'published' => true
    //         ], $this->getPageAccessToken($page_id))->getGraphNode()->asArray();
    //         return $videoId;
    //     } catch (FacebookSDKException $e) {
    //         dd($e); // handle exception
    //     }
    // }

    function publishTextStory($getdata)
    {
        $page_id = Auth::user()->facebook_page_id??'';
        try {
            $textPost = $this->api->post('/'.$page_id.'/feed', [
                'message' => $getdata->message,
                'published' => true
            ], $this->getPageAccessToken($page_id))->getGraphNode()->asArray();
            return $textPost;
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }
    function updatePost($getdata)
    {
        $page_id = Auth::user()->facebook_page_id??'';
        try {
            if ($getdata->post_type == 'video') {
                $type = 'description';
                $message = $getdata->message;
            } else{
                $type = 'message';
                $message = $getdata->message;
            }

            $post = $this->api->post('/' . $getdata->fb_post_id, array($type => $message), $this->getPageAccessToken($page_id));
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }



    public function publishToPage(Request $request){
        $page_id = Auth::user()->facebook_page_id??'';

        try {
            $id = $request->id;
            $photoIdArray = array();
            $getdata = Post::find($id);
            if ($getdata->fb_post_id != null) {

                $this->updatePost($getdata);
                $status_code= 200;
                $msg = 'Updated on facebook post successfully';
                $arr = array("status" => $status_code, "msg" => $msg);
            } else {
                if ($getdata->post_type == 'photo') {
                    if ($getdata->image != '') {
                        $photoIdArray = $this->uploadPhoto($getdata);
                        $post = $this->publishMultiPhotoStory($getdata->message, $photoIdArray);
                    }
                }elseif ($getdata->post_type == 'video') {
                    $post = $this->publishVideoStory($getdata);
                }elseif ($getdata->post_type == 'text') {
                    $post = $this->publishTextStory($getdata);
                }

                // dd($photoIdArray);
                $getdata->fb_post_id = $post['post_id']??$post['id'];
                $getdata->fb_id = $post['id'];
                $getdata->save();

                $status_code= 200;
                $msg = 'Created on facebook post successfully';
                $arr = array("status" => $status_code, "msg" => $msg);
            }

            return \Response::json($arr);

        } catch (\Throwable $th) {
            dd($th);
        }
    }
    // public function publishToPage(Request $request){
    //     $page_id = Auth::user()->facebook_page_id??'';

    //     try {
    //         if ($page_id && Auth::user()->token) {

    //             $id = $request->id;
    //             $getdata = Post::find($id);

    //             if (in_array($getdata->file_type, array("mp4", "mov", "wmv", "avi", "avchd", "flv","swf", "f4v", "mkv", "webm", "html5", "mpeg-2")))  {
    //                 $type1= 'videos';
    //                 $type2 = 'description';
    //             }else{
    //                 $type1 = 'photos';
    //                 $type2 = 'message';
    //             }

    //             if ($getdata->fb_post_id) {

    //                 $post = $this->api->post('/' . $getdata->fb_post_id, array($type2 => $getdata->message, 'source' => $this->api->fileToUpload(public_path('public/'.$getdata->image))), $this->getPageAccessToken($page_id));
    //             }else{
    //                 // dd($getdata->image);

    //                 foreach (json_decode($getdata->image) as $key => $val) {
    //                     $source[] = 'source'.$key;
    //                     $value[] = $this->api->fileToUpload(public_path('public/'.$val));
    //                 }
    //                 // dump($source[0]);
    //                 // dump($source[1]);
    //                 // dump($value[0]);
    //                 // dump($value[1]);
    //                 // dd('koi jasto');
    //                 $post = $this->api->post('/' . $page_id . '/' . $type1, array($type2 => $getdata->message, $source[0] => $value[0], $source[1] => $value[1]), $this->getPageAccessToken($page_id));
    //             }

    //             $post = $post->getGraphNode()->asArray();
    //             if (empty($getdata->fb_post_id)) {
    //                 if ($post) {
    //                     $getdata->fb_post_id = $post['post_id']??$post['id'];
    //                     $getdata->fb_id = $post['id'];
    //                     $getdata->save();
    //                     $status_code= 200;
    //                     $msg = 'Created on facebook post successfully';
    //                 }else{
    //                     $msg='your post was not created in facebook.';
    //                     $status_code= 400;
    //                 }
    //             }else{
    //                 if ($post['success'] == true) {
    //                     $msg = 'Updated on facebook post successfully';
    //                     $status_code= 200;
    //                 }else{
    //                     $msg = 'your post was not updated in facebook.';
    //                     $status_code= 400;
    //                 }
    //             }
    //         }else {
    //             if (empty(Auth::user()->token)) {
    //                 $msg='please generate  facebook token. > Go to <a href="'.url("/profile").'">profile</a>.';
    //             }else{
    //                 $msg='please add facebook page id > Go to <a href="'.url("/profile").'">profile</a>.';
    //             }
    //             $status_code= 400;
    //         }
    //         $arr = array("status" => $status_code, "msg" => $msg);
    //     } catch (\Illuminate\Database\QueryException $ex) {
    //         dd($ex);
    //         $msg = $ex->getMessage();
    //         if (isset($ex->errorInfo[2])) :
    //             $msg = $ex->errorInfo[2];
    //         endif;
    //         $status_code= 400;
    //         $arr = array("status" => 400, "msg" => $msg, "result" => array());
    //     } catch (Exception $ex) {
    //         dd($ex);
    //         if ($ex->getcode()== 100) {
    //             $id = decrypt($request->id);
    //             $getdata = Post::find($id);
    //             $getdata->fb_post_id = null;
    //             $getdata->fb_id = null;
    //             $getdata->save();
    //             $this->publishToPage($request->id);
    //         }
    //         $msg = $ex->getMessage();
    //         if (isset($ex->errorInfo[2])) :
    //             $msg = $ex->errorInfo[2];
    //         endif;
    //         $status_code= 400;
    //         $arr = array("status" => 400, "msg" => $msg, 'line'=> $ex->getLine(), "result" => array());
    //     }
    //     // return response()->json($arr, $status_code)->header('Content-Type', 'text/plain');
    //     return \Response::json($arr);
    // }



    public function getAllPost(){
        try {
            $page_id = Auth::user()->facebook_page_id;
            $params = "posts{full_picture,created_time,from,id,is_popular,is_published,message,comments,message_tags,target,targeting,updated_time,reactions,feed_targeting,properties,likes,attachments,subattachments.limit(10){title,media_type,media,description_tags,description,type,target,url},actions,child_attachments,sponsor_tags{access_token,ad_campaign,contact_address,description,fan_count,influences,is_always_open}},fan_count,new_like_count";
            $response = $this->api->get('/'.$page_id.'?fields='.$params, $this->getPageAccessToken($page_id))->getGraphNode()->asArray();
            $posts = @$response['posts'];
            if ($posts != null) {
                return view('admin.post.already_posted',compact('posts'));
            } else {
                Toastr::warning('Not post yet!', 'warning');
                return back();
            }

        } catch (\Throwable $th) {
            dd($th);
        }

    }
    public function postDetails($id){
        try {
            $page_id = Auth::user()->facebook_page_id;
            $params = "full_picture,created_time,message,attachments.limit(10){media,media_type,description,target,title,url,subattachments},comments{comments{created_time,message,from{name,picture,id},likes},created_time,message,from{name,picture,id},likes},likes,shares,status_type,updated_time,permalink_url,reactions,from,backdated_time,actions";
            $response = $this->api->get('/'.$id.'?fields='.$params, $this->getPageAccessToken($page_id))->getGraphUser()->asArray();
            $postDetail = $response;
            $getPageInfo = $this->pageInfoGet();

            return view('admin.post.already_post_details',compact('postDetail','getPageInfo'));
        } catch (\Throwable $th) {
            dd($th);
        }

    }
    public function individualPhotoDetails($id){
        try {
            $page_id = Auth::user()->facebook_page_id;
            $params = "created_time,name,picture,comments{comments{created_time,message,from{name,picture,id},likes},created_time,message,from{name,picture,id},likes},likes,updated_time,from,backdated_time";
            $response = $this->api->get('/'.$id.'?fields='.$params, $this->getPageAccessToken($page_id))->getGraphUser()->asArray();
            $postDetail = $response;
            $getPageInfo = $this->pageInfoGet();
            // dd($postDetail);
            return view('admin.post.individual_photo_details',compact('postDetail','getPageInfo'));
        } catch (\Throwable $th) {
            dd($th);
        }

    }
    public function deleteAlreadyPosted($post_id){
        try {
            $page_id = Auth::user()->facebook_page_id;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/v12.0/'.$post_id.'?access_token='.$this->getPageAccessToken($page_id));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close ($ch);

            $post = Post::where('fb_post_id',$post_id)->first();
            $post->delete();

            Toastr::success('Operation successful', 'Success');
            return redirect()->back();

        } catch (\Throwable $th) {
            dd($th);
        }

    }

    public function postCommentStore(Request $request)
    {
        // dump( $request->all());
        try {
            $page_id = Auth::user()->facebook_page_id;
            $comment = $this->api->post('/' . $request->post_id .'/comments', array('message' => $request->comment), $this->getPageAccessToken($page_id))->getGraphNode()->asArray();
            if($comment){
                Toastr::success('Operation successful', 'Success');
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            dd($th);
        }
    }

    public function commentReplyStore(Request $request)
    {
        // dd( $request->all());
        try {

            $page_id = Auth::user()->facebook_page_id;
            $reply = $this->api->post('/' . $request->comment_id .'/comments', array('message' => $request->reply), $this->getPageAccessToken($page_id))->getGraphNode()->asArray();
            if($reply){
                Toastr::success('Operation successful', 'Success');
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            dd($th);
        }
    }

    public function deleteComment($comment_id)
    {
        $page_id = Auth::user()->facebook_page_id;
        $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/v12.0/'.$comment_id.'?access_token='.$this->getPageAccessToken($page_id));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close ($ch);
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();

}

    public function userInfoGet($user_id){
        try {
            $page_id = Auth::user()->facebook_page_id;
            // $params = "id,first_name,last_name,middle_name,name,name_format,picture,short_name";
            $user_id =100010219595164;
            $params = "id,name,picture";
            $response = $this->api->get('/'.$user_id.'?fields='.$params)->getGraphUser();
            $userInfo = $response;
            dd($userInfo);
            return view('admin.post.already_post_details',compact('postDetail'));
        } catch (\Throwable $th) {
            dd($th);
        }

    }
    public function pageInfoGet(){
        try {
            $page_id = Auth::user()->facebook_page_id;
            $params = "id,name,picture";
            $response = $this->api->get('/'.$page_id.'?fields='.$params)->getGraphUser();
            return $response;
        } catch (\Throwable $th) {
            dd($th);
        }

    }

    // public function verify_token(Request $request)
    // {
    //     try {
    //         $mode  = $request->get('hub_mode');
    //         $token = $request->get('hub_verify_token');

    //         if ($mode && $token === env('WEBHOOK_VERIFY_TOKEN') && $request->get('hub_challenge')) {
    //             return response($request->get('hub_challenge'));
    //         }

    //         return response("Invalid token!", 400);
    //     } catch (\Throwable $th) {
    //         Log::info($th->getMessage());
    //     }

    // }

    // public function handle_query(Request $request)
    // {
    //     dd($request->all());
    //     try {
    //         $entry = $request->get('entry');

    //         $sender  = array_get($entry, '0.messaging.0.sender.id');
    //         dd($sender);
    //         // $message = array_get($entry, '0.messaging.0.message.text');

    //         $this->dispatchResponse($sender, 'Hello world. You can customise my response.');

    //         return response('', 200);
    //     } catch (\Throwable $th) {
    //         Log::info($th->getMessage());
    //     }

    // }

    // protected function dispatchResponse($id, $response)
    // {
    //     // $access_token = env('BOT_PAGE_ACCESS_TOKEN');
    //     try {
    //         $page_id = Auth::user()->facebook_page_id;
    //         $url = "https://graph.facebook.com/v2.6/me/messages?access_token=".$this->getPageAccessToken($page_id);

    //         $data = json_encode([
    //             'recipient' => ['id' => $id],
    //             'message'   => ['text' => $response]
    //         ]);

    //         $ch = curl_init($url);
    //         curl_setopt($ch, CURLOPT_POST, 1);
    //         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    //         curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    //         $result = curl_exec($ch);
    //         curl_close($ch);

    //         return $result;
    //     } catch (\Throwable $th) {
    //         Log::info($th->getMessage());
    //     }
    // }

    public function verify_token(Request $request)
    {
        try {
            $mode  = $request->get('hub_mode');
            $token = $request->get('hub_verify_token');
            Log::info('mode: '.$mode);
            Log::info('token: '.$token);

            if ($mode && $token === env('VERIFY_TOKEN') && $request->get('hub_challenge')) {
                Log::info('challange: '.$request->get('hub_challenge'));
                return response($request->get('hub_challenge'))->header('Content-Type', 'text/plain');

            }

            $data = $request->all();
            $id = $data['entry'][0]['messaging'][0]['sender']['id'];
            $message = $data['entry'][0]['messaging'][0]['message']['text'];
            Log::info("message:". $message);
            if(!empty($message)){
                $this->send_message($id, 'Hello Rayhan');
            }

            // return response("Invalid token!", 400);
        } catch (\Throwable $th) {
            Log::info($th->getMessage());
        }

    }

    public function send_message($id, $message)
        {
            Log::info('Yes');
            $page_id = Auth::user()->facebook_page_id;
            $url = 'https://graph.facebook.com/v2.6/me/messages?access_token='.$this->getPageAccessToken($page_id);
            $data = [
                'recipient' => [
                    'id' => $id
                ],
                'message' => [
                    'text' => $message
                ]
            ];
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_exec($ch);
            curl_close($ch);
        }
}
