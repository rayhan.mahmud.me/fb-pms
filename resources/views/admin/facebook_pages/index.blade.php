@extends('admin.master')
@section('style')
    <style>

    </style>
@endsection
@section('mainContent')
<div class="breadcrumb mt-4">
            <ul>
                <li><a href="#">Facebook Pages</a></li>
            </ul>
        </div>
    <div class="separator-breadcrumb border-top"></div>
    <div class="row">
        <div class="col-md-12 mb-3">
            <div class="card text-left">
                <div class="card-body">

                    <h4 class="card-title">Facebook Pages</h4>
                    <button type="button" style="float: right;" class="btn btn-primary" data-toggle="modal" data-target="#AddModal">+ Page</button>

                    <div class="table-responsive">
                        <table class="table test">
                            <thead>
                                <tr>
                                    <th scope="col">{{ __('ID') }}</th>
                                    <th scope="col">{{ __('Name') }}</th>
                                    <th scope="col">{{ __('Page ID') }}</th>
                                    <th scope="col">{{ __('Change Status') }}</th>
                                    <th scope="col">{{ __('Active Status') }}</th>
                                    <th scope="col">{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pages as $key=>$page)
                                    <tr>
                                        <th>{{ $key+1 }}</th>
                                        <td>{{ $page->name }}</td>
                                        <td>{{ $page->page_id }}</td>
                                        <td>
                                            <label class="switch pr-5 switch-success mr-3">
                                                <input type="checkbox" {{$page->active_status == 1 ?'checked':''}} value="{{ $page->id}}" onchange="update_active_status(this)"/><span class="slider"></span>
                                            </label>
                                        </td>
                                        <td >
                                            @if ($page->active_status == 1)
                                                <span class="badge badge-success">{{__('Active')}}</span>
                                            @else
                                                <span class="badge badge-danger">{{__('Not Active')}}</span>
                                            @endif

                                        </td>
                                        <td>
                                            <a class="btn btn-outline-success btn-icon m-1" href="javascript:void(0)" title="Edit" data-id="{{ $page->id }}" onclick="editPost(event.target)">{{ __('Edit') }}</a>
                                            <a class="btn btn-outline-danger btn-icon m-1 confirm-delete" href="#" title="Delete" data-href="{{route('fbPage.delete', $page->id)}}"><span class="ul-btn__icon"><i class="nav-icon i-Close-Window font-weight-bold"></i></span></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--  Add Modal content -->
    <div class="modal fade" id="AddModal" tabindex="-1" role="dialog" aria-labelledby="verifyModalContent" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="verifyModalContent_title">Add New Page</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <form  action="{{ route('fbPage.store') }}" method="POST" id="addForm" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-form-label" for="name">Page Name: <span class="text-danger">*</span></label>
                            <input class="form-control" value="{{old('name')}}" placeholder="Name" type="text" name="name" />
                        </div>
                        <div class="form-group">
                            <label class="col-form-label" for="page_id">Page ID: <span class="text-danger">*</span></label>
                            <input class="form-control" value="{{old('page_id')}}" type="text" name="page_id" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit" type="button">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!--  End Add Modal content -->
<!--  Edit Modal content -->
    <div class="modal fade" id="editModal" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="verifyModalContent_title">Edit Page</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form  action="{{ route('fbPage.update') }}" method="POST" id="updateForm" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-form-label" for="name">Page Name: <span class="text-danger">*</span></label>
                                <input class="form-control" id="name" placeholder="Name" type="text" name="name" />
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="page_id">Page ID: <span class="text-danger">*</span></label>
                                <input class="form-control" id="page_id" value="" type="text" name="page_id" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit" type="button">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!--  End Edit Modal content -->
@endsection

{{-- @section('modal')
    @include('backend.modal.delete_modal')
@endsection --}}

@section('script')
    <script>
        function editPost(event) {
        var id  = $(event).data("id");
        let _url = `admin/fb-page/edit/${id}`;

        $.ajax({
        url: _url,
        type: "GET",
        success: function(response) {
            if(response) {
                $("#id").val(response.id);
                $("#name").val(response.name);
                $("#page_id").val(response.page_id);
                $('#editModal').modal('show');
            }
        }
        });
    }
    </script>

    <script>
        $(document).ready(function() {
            $("#addForm").validate({
                rules: {
                    name: {
                        required: true,
                        maxlength: 100,
                    },
                    page_id: {
                        required: true,
                        maxlength: 3,
                        number: true
                    },

                },
                messages: {
                    name: {
                        required: "Name is required",
                        maxlength: "Name cannot be more than 100 characters"
                    },
                    page_id: {
                        required: "page_id is required",
                        maxlength: "page_id cannot be more than 3 characters",
                        number: "Must be a number"
                    },
                    native_name: {
                        required: "Native Name is required",
                        maxlength: "Native Name cannot be more than 150 characters"
                    }
                },
            });
            $("#updateForm").validate({
                rules: {
                    name: {
                        required: true,
                        maxlength: 100,
                    },
                    page_id: {
                        required: true,
                        maxlength: 3,
                        number: true
                    },

                },
                messages: {
                    name: {
                        required: "Name is required",
                        maxlength: "Name cannot be more than 100 characters"
                    },
                    page_id: {
                        required: "Page id is required",
                        maxlength: "Page id cannot be more than 3 characters",
                        number: "Must be a number"
                    },
                    native_name: {
                        required: "Native Name is required",
                        maxlength: "Native Name cannot be more than 150 characters"
                    }
                },
            });
        });


        function update_active_status(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('fbPage.update_active_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                   if(data == 1){

                    toastr.success(
                            "Operation Done Successfully",
                            "Success", {
                                timeOut: 5000,
                            }
                        );
                    $(".test").load(location.href + ' .test');
                }
                else{
                   toastr.warning(
                            "Something went wrong",
                            "Warning", {
                                timeOut: 5000,
                            }
                        );
                }
            });
        }

    </script>
@endsection
