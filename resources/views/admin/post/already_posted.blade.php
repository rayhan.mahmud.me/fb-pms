@extends('admin.master')

@section('mainContent')
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">Post</h5>
    <nav class="my-2 my-md-0 mr-md-3">
    </nav>
    <a href="{{ route('post.create') }}" class="btn btn-outline-primary openaddmodal">Add post</a>
</div>

<div class="separator-breadcrumb"></div>
    <div class="row">
        <div class="col-md-12 mb-3">
            <div class="card text-left">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table test">
        <thead>
            <tr>
                <th scope="col">SL</th>
                {{-- <th scope="col">Campaign Name</th> --}}
                <th scope="col">Message</th>
                <th scope="col">Image</th>
                <th scope="col">Link</th>
                <th scope="col">Type</th>
                <th scope="col">Video</th>
                <th scope="col">Created at</th>
                <th scope="col">Updated Time</th>
                <th scope="col">action</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($posts as $key=>$item)
            @php
                $attachment = @$item['attachments'][0];
                $like = $item['actions'][0];
                // dd($like);
                $comment = $item['actions'][1];
                $share = $item['actions'][2];
            @endphp
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ @$item['message'] }}</td>
                <td>
                    @if (@$attachment['type'] == 'album')
                        @foreach (@$item['attachments'][0]['subattachments'] as $img)
                            <img src="{{@$img['media']['image']['src'] }}" alt="" width="50px">
                        @endforeach
                    @else
                        <img src="{{@$attachment['media']['image']['src'] }}" alt="" width="50px">
                    @endif
                </td>
                <td><a href="{{  @$attachment['url'] }}" class="btn btn-primary" target="_blank" >View</a></td>
                <td>{{@$attachment['type'] }}</td>
                <td>
                    {{-- @dump($attachment['media_type']); --}}
                    @if (@$attachment['media_type'] == 'video' || @$attachment['type'] == 'video_inline')
                        <video width="55" height="50"controls>
                            <source src="{{@$attachment['media']['source'] }}" type="video/mp4">
                            <source src="{{@$attachment['media']['source'] }}" type="video/ogg">
                            <source src="{{@$attachment['media']['source'] }}" type="video/webm">
                            <object data="{{@$attachment['media']['source'] }}" width="470" height="255">
                            <embed src="{{@$attachment['media']['source'] }}" width="470" height="255">
                            </object>
                        </video>
                    @endif
                </td>
                <td>{{ $item['created_time']->format('Y-m-d H:i:s') }}</td>
                <td>{{ $item['updated_time']->format('Y-m-d H:i:s') }}</td>
                <td>
                    {{-- <a href="javascript:void(0)" data-toggle="modal" data-id="' . $id . '" data-target=".add_modal"
                        class="btn btn-success btn-sm openaddmodal"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" data-toggle="modal" data-id="' . $id . '"
                        class="btn btn-danger btn-sm delete_record"><i class="fas fa-trash-alt"></i></a>
                    <a href="javascript:void(0)" data-id="' . $id . '" class="btn btn-info btn-sm publishToProfile"><i
                            class="fab fa-facebook-square"></i></a>' --}}

                    {{-- <a class="btn btn-primary" href="{{ route('post.edit',$item->id) }}"><i
                            class="fa fa-pencil-square-o" title="Edit" aria-hidden="true"></i></a> --}}
                    <a class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{@$item->id}}" href="#"
                        class="disabled"><i class="fa fa-trash" title="Delete" aria-hidden="true"></i></a>
                    <a class="btn btn-info" onclick="publishToProfile({{@$item->id}})" href="#"><i class="fa fa-facebook"
                            title="Post to Facebook" aria-hidden="true"></i></a>

                    <a class="btn btn-success" href="{{ route('fbPostDetails',$item['id']) }}">Details</a>
                </td>
            </tr>

            {{-- Delete Modal--}}
            <div class="modal fade" id="deleteModal{{@$item->id}}" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel-2" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Delete Post</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body">
                            <div class="text-center">
                                <h4>Are You Sure To Delete ?</h4>
                            </div>

                            <div class="mt-20 d-flex justify-content-between">
                                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                                {{-- <form action="{{url('admin/payment-delete/'.@$item->id)}}" method="get"
                                    enctype="multipart/form-data"> --}}
                                    <a href="{{url('admin/delete-already-posted/'.@$item['id'])}}"><button
                                            class="btn btn-danger">Delete</button></a>
                                </form>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            {{-- End Delete Modal --}}

            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
</div>
</div>


@endsection
