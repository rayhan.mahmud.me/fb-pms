@extends('admin.master')

@section('mainContent')
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">Post</h5>
    <nav class="my-2 my-md-0 mr-md-3">
    </nav>
    <a href="{{ route('post.create') }}" class="btn btn-outline-primary openaddmodal">Add post</a>
</div>

<div class="separator-breadcrumb"></div>
    <div class="row">
        <div class="col-md-12 mb-3">
            <div class="card text-left">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table test">
                            <thead>
                                <tr>
                                    <th scope="col">SL</th>
                                    <th scope="col">Campaign Name</th>
                                    <th scope="col">Message</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Link</th>
                                    <th scope="col">Video</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Created at</th>
                                    <th scope="col">Updated Time</th>
                                    <th scope="col">action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $key=>$item)
                                    {{-- @dd($item) --}}
                                    <tr>
                                        <td>{{ $key + 1 + ($posts->currentPage() - 1) * $posts->perPage() }}</td>
                                        <td>{{ @$item->name }}</td>
                                        <td>{{ @$item->message }}</td>
                                        <td>
                                            @if ($item->image)
                                                @foreach (json_decode(@$item->image) as $img)
                                                    <img src="{{ uploaded_asset($img) }}" alt="" width="50px">
                                                @endforeach
                                            @endif

                                        </td>
                                        <td>{{ @$item->link }}</td>
                                        <td>
                                            @if ($item->video)
                                                <video width="55" height="50"controls>
                                                    <source src="{{ uploaded_asset($item->video) }}" type="video/mp4">
                                                    <source src="{{ uploaded_asset($item->video) }}" type="video/ogg">
                                                    <source src="{{ uploaded_asset($item->video) }}" type="video/webm">
                                                    <object data="{{ uploaded_asset($item->video) }}" width="470" height="255">
                                                    <embed src="{{ uploaded_asset($item->video) }}" width="470" height="255">
                                                    </object>
                                                </video>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($item->fb_post_id != null)
                                                <span class="badge badge-success">Already Post</span>
                                            @else
                                                <span class="badge badge-danger">Pending</span>
                                            @endif
                                        </td>
                                        <td>{{ @$item->created_at }}</td>
                                        <td>{{ @$item->updated_at }}</td>
                                        <td>
                                            @php
                                                $title = '';
                                                if ($item->fb_post_id != null) {
                                                    $title = 'Update Post to Facebook';
                                                }else {
                                                    $title = 'Post to Facebook';
                                                }

                                            @endphp
                                            {{-- <a href="javascript:void(0)" data-toggle="modal" data-id="' . $id . '" data-target=".add_modal"
                                                class="btn btn-success btn-sm openaddmodal"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="javascript:void(0)" data-toggle="modal" data-id="' . $id . '"
                                                class="btn btn-danger btn-sm delete_record"><i class="fas fa-trash-alt"></i></a>
                                            <a href="javascript:void(0)" data-id="' . $id . '" class="btn btn-info btn-sm publishToProfile"><i
                                                    class="fab fa-facebook-square"></i></a>' --}}

                                            <a class="btn btn-primary" href="{{ route('post.edit',$item->id) }}"><i
                                                    class="fa fa-pencil-square-o" title="Edit" aria-hidden="true"></i></a>
                                            @if ($item->fb_post_id == null)
                                                <a class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{@$item->id}}" href="#"
                                                class="disabled"><i class="fa fa-trash" title="Delete" aria-hidden="true"></i></a>
                                            @endif
                                            {{-- <a class="btn btn-info" onclick="publishToProfile({{@$item->id}})" href="#"><i class="fa fa-facebook"
                                                    title="Post to Facebook" aria-hidden="true"></i></a> --}}

                                                    <a class="btn btn-info ladda-button basic-ladda-button text-white" data-style="expand-right" onclick="publishToProfile({{@$item->id}})"><i class="fa fa-facebook"
                                                        title="{{ @$title }}" aria-hidden="true"></i></a>

                                            {{-- <a class="btn btn-success" href="{{ route('postDetails',$item->id) }}">Details</a> --}}
                                        </td>
                                    </tr>

                                    {{-- Delete Modal--}}
                                    <div class="modal fade" id="deleteModal{{@$item->id}}" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalLabel-2" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Delete Post</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>

                                                <div class="modal-body">
                                                    <div class="text-center">
                                                        <h4>Are You Sure To Delete ?</h4>
                                                    </div>

                                                    <div class="mt-20 d-flex justify-content-between">
                                                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                                                        {{-- <form action="{{url('admin/payment-delete/'.@$item->id)}}" method="get"
                                                            enctype="multipart/form-data"> --}}
                                                            <a href="{{url('admin/post/destroy/'.@$item->id)}}"><button
                                                                    class="btn btn-danger">Delete</button></a>
                                                        </form>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Delete Modal --}}
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix">
                        <div class="pull-right">
                            {{ $posts->appends(request()->input())->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
<script>
    // pms
//     $(document).ready(function() {
//     // $(".datatable").DataTable({
//     // processing: true,
//     // serverSide: true,
//     // bPaginate: true,
//     // ajax: {
//     // url: "{{ route('getall') }}",
//     // type: "POST",
//     // data: function(d) {
//     // d._token = "{{ csrf_token() }}";
//     // },
//     // },
//     // columns: [
//     // { data: "DT_RowIndex", orderable: false },
//     // { data: "image" },
//     // { data: "name" },
//     // { data: "message" },
//     // { data: "status" },
//     // { data: "action", orderable: false, width: "15%" },
//     // ],
//     // });

//     $("body").on("click", ".openaddmodal", function() {
//     var id = $(this).data("id");
//     if (id == "") {
//     $(".modal-title").text("Create");
//     } else {
//     $(".modal-title").text("Update");
//     }
//     $.ajax({
//     url: "{{ route('getmodal')}}",
//     type: "POST",
//     headers: {
//     "X-CSRF-TOKEN": "{{ csrf_token() }}",
//     },
//     data: { id: id },
//     success: function(data) {
//     $(".addbody").html(data);
//     $(".add_modal").modal("show");
//     },
//     });
//     });

//     $("body").on("submit", ".formsubmit", function(e) {
//     e.preventDefault();
//     $.ajax({
//     url: $(this).attr("action"),
//     data: new FormData(this),
//     type: "POST",
//     contentType: false,
//     cache: false,
//     processData: false,
//     beforeSend: function() {
//     $(".spinner").html('<i class="fa fa-spinner fa-spin"></i>');
//     },
//     success: function(data) {
//     if (data.status == 400) {
//     $(".spinner").html("");
//     }
//     if (data.status == 200) {
//     $(".spinner").html("");
//     $(".add_modal").modal("hide");

//     $("#datatable").DataTable().ajax.reload();
//     }
//     },
//     });
//     });


//     $(".publishToProfile").on("click", function() {
//         var url = $('meta[name="url"]').attr("content");
//         var id = $(this).data("id");
//         $.ajax({
//         url: url + "/admin/page",
//         type: "POST",
//         headers: {
//             "X-CSRF-TOKEN": "{{ csrf_token() }}",
//         },
//         data: { id: id },
//             success: function(data) {
//             if (data.status == 200) {
//                 alert(data);

//                 $.confirm({
//                     title: "Success!",
//                     content: data.msg,
//                     autoClose: "cancelAction|3000",
//                     buttons: {
//                         cancelAction: function(e) {},
//                     },
//                 });
//             }
//             if (data.status == 400) {
//                 alert(data);

//                 $.alert({
//                     title: "Alert!",
//                         content: data.msg,
//                     });
//                 }
//             },
//         });
//     });
// });

function publishToProfile(post_id) {
    console.log(post_id);
    $.ajax({
        url: "{{ route('publishToPage') }}",
        type: "POST",
        headers: {
            // 'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            "X-CSRF-TOKEN": "{{ csrf_token() }}",
        },
        data: { id: post_id },
            success: function(data) {
                console.log(data);
            if (data.status == 200) {
                toastr.success(
                    data.msg,
                    "Success", {
                        timeOut: 5000,
                    }
                );
            }
            if (data.status == 400) {
                toastr.warning(
                     data.msg,
                    "Warning", {
                        timeOut: 5000,
                    }
                );
            }
        },

});
}
</script>
@endsection
