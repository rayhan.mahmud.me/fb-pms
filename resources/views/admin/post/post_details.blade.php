@extends('admin.master')

@section('mainContent')
<div class="d-flex flex-column align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <p>{{ @$postDetail->message }}</p>
    <p>{{ @$postDetail->message_tags }}</p>
    <img src="{{ @$postDetail->full_picture }}" alt="" height="300px" width="100%">
    <p>Posted At: {{ get_time_difference_php(@$postDetail->created_time) }}</p>
</div>

<div class="container">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="comments">
                    <div class="comments-details">
                        <span class="total-comments comments-sort">117 Comments</span>
                    </div>
                    <div class="comment-box add-comment">
                        <span class="commenter-pic">
                            <img src="https://source.unsplash.com/mEZ3PoFGs_k/300*300" class="img-fluid">
                        </span>
                        <span class="commenter-name">
                            <form action="{{ route('postCommentStore') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="post_id" value="{{ @$postDetail->id }}">
                                <input type="text" placeholder="Add a public comment" name="comment">
                                <button type="submit" class="btn btn-default">Comment</button>
                                {{-- <button type="cancel" class="btn btn-default">Cancel</button> --}}
                            </form>
                        </span>
                    </div>
                    {{-- @dd($postDetail->comments) --}}
                    @foreach (@$postDetail->comments->data as $comment)

                    <div class="comment-box">
                        <span class="commenter-pic">
                            <img src="https://source.unsplash.com/mEZ3PoFGs_k/300*300" class="img-fluid">
                        </span>
                        <span class="commenter-name">
                            <a href="#">{{ @$comment->from->name }}</a> <span class="comment-time">
                                {{ get_time_difference_php(@$comment->created_time) }}
                            </span>
                        </span>
                        <p class="comment-txt more">{{ $comment->message }}{{ $comment->id }}</p>
                        <div class="comment-meta">
                            <a class="comment-like" href="{{ route('deleteComment',$comment->id) }}">Delete</a>
                            <button class="comment-like"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                99</button>
                            <button class="comment-dislike"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                                149</button>
                        </div>


                        @php
                        $ch = curl_init();

                        curl_setopt($ch, CURLOPT_URL,
                        'https://graph.facebook.com/v12.0/'.$comment->id.'?fields=from%2Cmessage%2Ccomments&access_token='.Auth::user()->token);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                        $result = curl_exec($ch);
                        $data = json_decode($result);
                        if (curl_errno($ch)) {
                        echo 'Error:' . curl_error($ch);
                        }
                        curl_close($ch);
                        $commentReplies = $data;
                        @endphp
                        @if (isset($commentReplies->comments))

                        @foreach ($commentReplies->comments->data as $reply)
                        <div class="comment-box replied">
                            <span class="commenter-pic">
                                <img src="https://source.unsplash.com/mEZ3PoFGs_k/300*300" class="img-fluid">
                            </span>
                            <span class="commenter-name">
                                <a href="#">{{ @$reply->from->name }}</a> <span class="comment-time">
                                    {{ get_time_difference_php(@$reply->created_time) }}
                                </span>
                            </span>
                            <p class="comment-txt more">{{ @$reply->message }}{{ $reply->id }}</p>
                            <div class="comment-meta">
                                <a class="comment-like" href="{{ route('deleteComment',$reply->id) }}">Delete</a>
                                <button class="comment-like"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                    99</button>
                                <button class="comment-dislike"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                                    149</button>
                            </div>

                        </div>
                        @endforeach
                        @endif
                        <div class="comment-box add-comment">
                            <span class="commenter-pic">
                                <img src="https://source.unsplash.com/mEZ3PoFGs_k/300*300" class="img-fluid">
                            </span>
                            <span class="commenter-name">
                                <form action="{{ route('commentReplyStore') }}" method="post"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="comment_id" value="{{ $comment->id }}">
                                    <input type="text" placeholder="Add a public reply" name="reply">
                                    <button type="submit" class="btn btn-default">Reply</button>
                                    {{-- <button type="cancel" class="btn btn-default reply-popup">Cancel</button> --}}
                                </form>
                            </span>
                        </div>
                    </div>
                    @endforeach
                    {{-- <div class="comment-box">
                        <span class="commenter-pic">
                            <img src="https://source.unsplash.com/mEZ3PoFGs_k/300*300" class="img-fluid">
                        </span>
                        <span class="commenter-name">
                            <a href="#">Happy markuptag</a> <span class="comment-time">2 hours ago</span>
                        </span>
                        <p class="comment-txt more">Suspendisse massa enim, condimentum sit amet maximus quis, pulvinar
                            sit
                            amet ante. Fusce eleifend dui mi, blandit vehicula orci iaculis ac.</p>
                        <div class="comment-meta">
                            <button class="comment-like"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                99</button>
                            <button class="comment-dislike"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                                149</button>
                            <button class="comment-reply"><i class="fa fa-reply-all" aria-hidden="true"></i>
                                Reply</button>
                        </div>
                        <div class="comment-box replied">
                            <span class="commenter-pic">
                                <img src="https://source.unsplash.com/mEZ3PoFGs_k/300*300" class="img-fluid">
                            </span>
                            <span class="commenter-name">
                                <a href="#">Happy markuptag</a> <span class="comment-time">2 hours ago</span>
                            </span>
                            <p class="comment-txt more">Suspendisse massa enim, condimentum sit amet maximus quis,
                                pulvinar
                                sit amet ante. Fusce eleifend dui mi, blandit vehicula orci iaculis ac.</p>
                            <div class="comment-meta">
                                <button class="comment-like"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                    99</button>
                                <button class="comment-dislike"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                                    149</button>
                                <button class="comment-reply"><i class="fa fa-reply-all" aria-hidden="true"></i>
                                    Reply</button>
                            </div>
                            <div class="comment-box replied">
                                <span class="commenter-pic">
                                    <img src="https://source.unsplash.com/mEZ3PoFGs_k/300*300" class="img-fluid">
                                </span>
                                <span class="commenter-name">
                                    <a href="#">Happy markuptag</a> <span class="comment-time">2 hours ago</span>
                                </span>
                                <p class="comment-txt more">Suspendisse massa enim, condimentum sit amet maximus quis,
                                    pulvinar sit amet ante. Fusce eleifend dui mi, blandit vehicula orci iaculis ac.</p>
                                <div class="comment-meta">
                                    <button class="comment-like"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                        99</button>
                                    <button class="comment-dislike"><i class="fa fa-thumbs-o-down"
                                            aria-hidden="true"></i>
                                        149</button>
                                    <button class="comment-reply"><i class="fa fa-reply-all" aria-hidden="true"></i>
                                        Reply</button>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    // Reply box popup JS
    $(document).ready(function(){
    $(".reply-popup").click(function(){
    $(".reply-box").toggle();
    });
    });
</script>
@endpush
@endsection