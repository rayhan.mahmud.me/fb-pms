@extends('admin.master')
@section('style')
    <style>
        video {
            border: 1px solid black;
            display: block;
        }
    </style>
@endsection
@section('mainContent')
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">Post @if (@$editData) Edit @else Create @endif</h5>
    <nav class="my-2 my-md-0 mr-md-3">
    </nav>
    <a href="{{ route('post.index') }}" class="btn btn-outline-primary openaddmodal">Post List</a>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card mb-5 ">
            <div class="card-body col-md-12">
                <div class="mb-40">
                    {{-- {{dd(@$data)}} --}}
                    @if (@$editData)
                    <form method="POSt" action="{{route('post.update',@$editData->id)}}" enctype="multipart/form-data">
                        @else
                        <form method="POST" action="{{route('post.store')}}" enctype="multipart/form-data">
                            @endif
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">

                                    <div class="white-box">
                                        <div class="">
                                            <div class="col-lg-8">
                                                <div class="input-effect">
                                                    <label>Campaign Name <span>*</span> </label>
                                                    <input
                                                        class="primary-input form-control {{$errors->has('name') ? 'is-invalid' : ' '}}"
                                                        type="text" name="name"
                                                        value="{{isset($editData)? @$editData->name : '' }}">
                                                    <span class="focus-border"></span>

                                                    @if ($errors->has('name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-8 mb-4">
                                                <div class="input-effect">
                                                    <label>Message <span>*</span> </label>
                                                    <textarea
                                                        class="primary-input form-control {{ $errors->has('message') ? 'is-invalid' : ''}}"
                                                        cols="0" rows="6" name="message"
                                                        id="message">{{isset($editData)? @$editData->message : '' }}</textarea>
                                                    <span class="focus-border textarea"></span>
                                                    @if ($errors->has('message'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('message') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            @if (!@$editData)
                                                <div class="col-lg-4 mb-4">
                                                    <label>Post Type</label>
                                                    <select name="post_type" class="form-control" id="post_type">
                                                        <option value="">Select Type</option>
                                                        <option value="photo">Photo</option>
                                                        <option value="video">Video</option>
                                                    </select>
                                                </div>
                                            @endif



                                            @if (isset($editData))
                                            <img height="80px;" width="100px;" src="{{ uploaded_asset($editData->image) }}"
                                                class="img img-fluid photo_div">
                                            @endif
                                            {{-- <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label> Image </label>
                                                    <input type="file" name="image" class="file-upload-default">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control file-upload-info"
                                                            disabled placeholder="Upload Image">
                                                        <span class="input-group-append">
                                                            <button class="file-upload-browse btn btn-primary"
                                                                type="button">Upload</button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div> --}}

                                            {{-- // --}}
                                            <div class="col-lg-8 photo_div">
                                                <div class="input-group hdtuto control-group lst increment" >
                                                    <input type="file" name="filenames[]" class="myfrm form-control">
                                                    <div class="input-group-btn">
                                                    <button class="btn btn-success" onclick="addRow()" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 photo_div">
                                              <div class="clone hide">
                                                <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                                                  <input type="file" name="filenames[]" class="myfrm form-control">
                                                  <div class="input-group-btn">
                                                    <button class="btn btn-danger" onclick="removeRow()" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            {{-- // --}}

                                            <video id="video" class=" video_div" width="100" height="100" controls></video>
                                            <div class="col-lg-8 video_div">
                                                <div class="form-group">
                                                    <label> Video </label>
                                                    <input id="file-input" type="file" name="video" accept="video/*" class="file-upload-default">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control file-upload-info"
                                                            disabled placeholder="Upload Video">

                                                        <span class="input-group-append">
                                                            <button class="file-upload-browse btn btn-primary"
                                                                type="button">Upload</button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-40">
                                    <div class="col-lg-12 text-center align-item-center justify-content-center">
                                        <button class="btn btn-primary">
                                            <span class="ti-check"></span>
                                            @if(isset($editData))
                                            Update
                                            @else
                                            Save
                                            @endif
                                            Post
                                        </button>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
</div>

@endsection

@section('script')

    <script>
        // document ready function
        $(document).ready(function () {
            $('.link_div').hide();
            $('.video_div').hide();
            $('.photo_div').hide();
        });

        // onchage post_type
        $('#post_type').on('change', function() {
            var post_type = $(this).val();
            if (post_type == 'photo') {
                $('.photo_div').show();
                $('.video_div').hide();
                $('.link_div').hide();
            } else if (post_type == 'video') {
                $('.video_div').show();
                $('.photo_div').hide();
                $('.link_div').hide();
            } else if (post_type == 'link') {
                $('.link_div').show();
                $('.video_div').hide();
                $('.photo_div').hide();
            }else{
                $('.link_div').hide();
                $('.video_div').hide();
                $('.photo_div').hide();
            }

        });
        const input = document.getElementById('file-input');
        const video = document.getElementById('video');
        const videoSource = document.createElement('source');

        input.addEventListener('change', function() {
        const files = this.files || [];

        if (!files.length) return;

        const reader = new FileReader();

        reader.onload = function (e) {
            videoSource.setAttribute('src', e.target.result);
            video.appendChild(videoSource);
            video.load();
            video.play();
        };

        reader.onprogress = function (e) {
            console.log('progress: ', Math.round((e.loaded * 100) / e.total));
        };

        reader.readAsDataURL(files[0]);
        });
    </script>
    <script type="text/javascript">

        function addRow() {
            var newdiv = $('.clone').html();
            $('.increment').append(newdiv);
        }
        function removeRow() {
            $('.increment').find('.hdtuto').last().remove();
        }
    </script>
@endsection
