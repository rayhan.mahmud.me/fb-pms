@extends('admin.master')
@section('style')
<style>
    .gallery {
  width: 1200px;
  margin: 0 auto;
  padding: 5px;
  background: #fff;
  box-shadow: 0 1px 1px rgba(0,0,0,.1);
}

.gallery > a > div {
  position: relative;
  float: left;
  padding: 5px;
}

.gallery > a > div > img {
  width: 250px;
  transition: .1s transform;
  transform: translateZ(0); /* hack */
}

.gallery > a > div:hover {
  z-index: 1;
}

.gallery > a > div:hover > img {
  transform: scale(1.1,1.1);
  transition: .3s transform;
}

.cf:before, .cf:after {
  display: table;
  content: "";
  line-height: 0;
}

.cf:after {
  clear: both;
}

h1 {
  margin: 40px 0;
  font-size: 24px;
  text-align: center;
}
</style>

@endsection
@section('mainContent')
<div class="d-flex flex-column align-items-center p-3 px-md-4 mb-3 box-shadow">
    <p>{{ @$postDetail['name'] }}</p>
    <div class="mb-1 text-muted">Created At: {{get_time_difference_php(@$postDetail['created_time']->format('Y-m-d H:i:s'))}}</div>
    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#exampleModal">
        Live Preview
    </button>
    {{-- <img src="{{ @$postDetail['picture'] }}" alt="" height="300px" width="100%"> --}}
    <div class="gallery cf">
        <a href="#">
            <div>
                <img src="{{ @$postDetail['picture'] }}" height="250px" width="100%" />
            </div>
        </a>
    </div>
    {{-- <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D{{ @$postDetail['id'] }}%26id%3D111861468008931&show_text=true&width=300" width="300" height="445" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe> --}}
    {{-- <p>Posted At: {{ get_time_difference_php(@$postDetail['created_time']->format('Y-m-d H:i:s')) }}</p> --}}
</div>
<div class="container">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="comments">
                    <div class="comments-details">
                        <span class="total-comments comments-sort">Comments</span>
                    </div>
                    <div class="comment-box add-comment">
                        <span class="commenter-pic">
                            <img src="{{ $getPageInfo['picture']['url'] }}" class="img-fluid">
                        </span>
                        <span class="commenter-name">
                            <form action="{{ route('postCommentStore') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="post_id" value="{{ @$postDetail['id'] }}">
                                <input type="text" placeholder="Add a public comment" data-emoji-picker="true" name="comment">
                                <button type="submit" class="btn btn-default">Comment</button>
                                {{-- <button type="cancel" class="btn btn-default">Cancel</button> --}}
                            </form>
                        </span>
                    </div>
                    @if (@$postDetail['comments'])

                        @foreach (@$postDetail['comments'] as $comment)
                        <div class="comment-box">
                            <span class="commenter-pic">
                                <img src="{{ @$comment['from']['picture']['url'] }}" class="img-fluid">
                            </span>
                            <span class="commenter-name">
                                @if (@$comment['from']['id'] != null)
                                    <a href="{{ route('userInfoGet', @$comment['from']['id']) }}">{{ @$comment['from']['name'] }}</a>
                                @endif
                                <span class="comment-time">
                                    {{ get_time_difference_php(@$comment['created_time']->format('Y-m-d H:i:s')) }}
                                </span>
                            </span>
                            <div class="comment-meta">
                                <a class="comment-like text-danger" href="{{ route('deleteComment',$comment['id']) }}">Delete</a>
                                <button class="comment-like"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                    @if (@$comment['likes'] && sizeof(@$comment['likes']) > 0)
                                        {{ sizeof(@$comment['likes']) }}
                                    @else
                                        0
                                    @endif
                                </button>
                            </div>
                            <p class="comment-txt more">{{ $comment['message'] }}</p>

                            @if (@$comment['comments'])
                                @foreach ($comment['comments'] as $reply)
                                <div class="comment-box replied">
                                    <span class="commenter-pic">
                                        <img src="{{ @$reply['from']['picture']['url'] }}" class="img-fluid">
                                    </span>
                                    <span class="commenter-name">
                                        <a href="{{ @$reply['from']['id'] }}">{{ @$reply['from']['name'] }}</a> <span class="comment-time">
                                            {{ get_time_difference_php(@$reply['created_time']->format('Y-m-d H:i:s')) }}
                                        </span>
                                    </span>
                                    <div class="comment-meta">
                                        <a class="comment-like text-danger" href="{{ route('deleteComment',$reply['id']) }}">Delete</a>
                                        <button class="comment-like"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                            @if (@$reply['likes'] && sizeof(@$reply['likes']) > 0)
                                                {{ sizeof(@$reply['likes']) }}
                                            @else
                                                0
                                            @endif

                                        </button>
                                    </div>
                                    <p class="more">{{ @$reply['message'] }}</p>

                                </div>
                                @endforeach
                            @endif



                            <div class="comment-box add-comment">
                                <span class="commenter-pic">
                                    <img src="{{ $getPageInfo['picture']['url'] }}" class="img-fluid">
                                </span>
                                <span class="commenter-name">
                                    <form action="{{ route('commentReplyStore') }}" method="post"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="comment_id" value="{{ $comment['id'] }}">
                                        <input type="text" placeholder="Add a public reply" data-emoji-picker="true" name="reply">
                                        <button type="submit" class="btn btn-default">Reply</button>
                                        {{-- <button type="cancel" class="btn btn-default reply-popup">Cancel</button> --}}
                                    </form>
                                </span>
                            </div>
                        </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
<!--  Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Post Live Preview</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D{{ @$postDetail['id'] }}%26id%3D111861468008931&show_text=true&width=450" width="450" height="600" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
            </div>
        </div>
    </div>
</div>
<!--  Modal -->

@push('script')
<script>
    // Reply box popup JS
    $(document).ready(function(){
    $(".reply-popup").click(function(){
    $(".reply-box").toggle();
    });
    });
</script>
@endpush
@endsection
